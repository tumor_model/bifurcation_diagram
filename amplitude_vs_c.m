%This function will look at the three different sections in our system
%where we have oscillations. It will then find the amplitude of the
%oscillations and plot the maximum amplitude vs. antigenicity (c).
% There are oscillations from C0 = 8.55 * 10^-5 to C1(our hopf bifurcation.
% For our values in Table 1, C1 is at ).032. Kenny 3/25/17


% Fixed parameters from Table 1 on page 138.
tumor_p.mu_2 = 0.03;
tumor_p.p_1 = 0.1245;
tumor_p.g_1 = 20000000;
tumor_p.g_2 = 100000;
tumor_p.r_2 = 0.18;
tumor_p.b = 0.000000001;
tumor_p.a = 1;
tumor_p.mu_3 = 10;
tumor_p.p_2 = 5;
tumor_p.g_3 = 1000;

% Add s_1 and s_2 to the tumor parameters. These are zero for now.
tumor_p.s_1 = 0;
tumor_p.s_2 = 0;

% Set the initial values and the desired t-span
y0 = [1,1,1];
%tspan = 0:100;
tspan = [0, 10000]; % Initially, this was only 100 which means we never even saw
% peaking behavior for some values of c

%Instantiate our lists
effector_amp_list = [];
tumor_amp_list = [];
Il2_amp_list = [];
%Iterating through each value of c and finding the max amplitude at each
%value of c

%c_list = 0.0000856:0.00001:0.031;
c_list = 0:0.001:0.05;
% Changed the domain to match every other plot we are showing. I know the
% tail end might not be as interesting, but if we don't show it, then
% people will ask and we will have to waste time talking about boring
% stuff. LAME! Vera 3/26/17.

% Preallocate instead of concatenate, it gives better run time
effector_amp = nan(1, length(c_list));
effector_period = nan(1, length(c_list));
tumor_amp = nan(1, length(c_list));
tumor_period = nan(1, length(c_list));
il2_amp = nan(1, length(c_list));
il2_period = nan(1, length(c_list));

%for c = 0.0000856:0.00001:0.031
% 1) no need to re-instantiate c, that's not necessary and makes creating
% c_vals up above pointless.
% 2) Also, I realized you were using c as your iterator variable, that is
% sortof like iterating by element which is done in Python but I've never
% seen it in Matlab. Don't use a variable that has meaning as the iterator.
% I often use i. Vera 3/26/17.

% Check accuracy by capturing plots as we go through
%fig1 = figure;
%hold on

for i = 1:length(c_list)
    
    % Now that c isn't the iterator, we can add it to tumor_p
    c = c_list(i);
    
    %[t, y] = ode45(@(t,y) ODE_system(t, y, c),t_span, y0);
    [t, y] = ode15s(@(t, y) ODE_system(t, y, c, tumor_p), tspan, y0);
    
    % Do things in a loop to save repitition
    for n = 1:3
        
        value_column = y(:,n);
        
        % Get the period
        [~, peak_indices] = find_peaks(value_column);
        
        %Calculate the average time between peaks
        % Preallocate
        differences = nan(1, length(peak_indices));
        
        previous_peak_time = t(peak_indices(1));
        for j = 2:length(peak_indices)
            peak_time = t(peak_indices(j));
            differences(j) = peak_time - previous_peak_time;
        end
        
        % Get the mean (average) period
        period = nanmean(differences);
        
        % Remove zeros, and then get the amplitude
        % There should NEVER be negative values, this is a sign of instability.
        % Initially, I tried neglecting such values, but then I had NO values.
        % So, I replaced everything in the vectors that was less than zero with
        % zero. Vera 3/26/17.
        value_column(value_column < 0) = 0;
        
        % Get the amplitude
        amplitude = max(value_column) - min(value_column);
        
        % Save the values in the appropriate struct array field
        switch n
            case 1
                effector_amp(i) = amplitude;
                effector_period(i) = period;
            case 2
                tumor_amp(i) = amplitude;
                tumor_period(i) = period;
            case 3
                il2_amp(i) = amplitude;
                il2_period(i) =  period;
        end
    end
end

%The effector cells figure vs. c
effector_figure = figure;
plot(c_list, effector_amp)
title('Effector Cells vs. Antigenicity (c)');
xlabel('Antigenicity (c)');
ylabel('Max Amplitude of Effector Cell Oscillations');

% Tumor Cells vs. C
tumor_figure = figure;
plot(c_list, tumor_amp)
title('Tumor Cells vs. Antigenicity (c)');
xlabel('Antigenicity (c)');
ylabel('Max Amplitude of Tumor Cell Oscillations');

%IL-2 vs. c
Il2_figure = figure;
plot(c_list, il2_amp)
title('IL-2 vs. Antigenicity (c)');
xlabel('Antigenicity (c)');
ylabel('Max Amplitude of IL-2 Oscillations');

% Plot the periods

%The effector cells figure vs. c
fig4 = figure;
plot(c_list, effector_period)
title('Period vs. Antigenicity (c)');
xlabel('Antigenicity (c)');
ylabel('Period in Days');
% Note that all factors seem to have the same period with different
% amplitudes, so I am plotting just the first item



